all:
	cppcheck --enable=all --inconclusive main.cpp
	g++ -O3 main.cpp -Wall -Wextra -Weffc++ -lgsl -lcblas
	@echo "..................................................."
	time ./a.out > q
	rm a.out
	@echo "..................................................."
	tail -n 40 q

tex:
	rm -f math.pdf
	pdflatex math.tex
	evince math.pdf 

test:
	cppcheck --enable=all --inconclusive main.cpp
	g++ -O3 main.cpp -Wall -Wextra -Weffc++ -lgsl -lcblas
	@echo "compilation"
	time ./a.out > testdump
	@echo "result"
	tail -n 40 testdump
	@echo ""
