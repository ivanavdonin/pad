#include <iostream>
#include <cmath>
#include <complex>
#include <boost/math/special_functions/spherical_harmonic.hpp>
#include <boost/math/special_functions/bessel.hpp>
//#include <algorithm> 
//#include <stdlib.h>
//# define PI 3.141592653589793238462643383279502884 /* pi */
#define PI M_PI

//input: rA, rB, r, l, m
double rA[3] = {1,2,3};
double rB[3] = {4,5,6};
//double rA[3] = {0.,0.,0.};
//double rB[3] = {0.,0.,0.};
//double r[3] = {7,8,9};
double alpha = 0.2;
double epsilon = 10e-5;
int l = 5;
int m = 3;
int infty = 100;

double fact(double n){
	//assert(n >= 0);
	if (n < 0) { return 0; }
	return (double)tgamma(n+1);		
}

double CG(int j1, int m1, int j2, int m2, int J, int M ){
	if ((M != m1 + m2) && (j1>0) )
		return 0;
	else {
		double a;
		double b;
		int c1, c2, c3, c4, c5;
		double c = 0;
		a = sqrt((2*J+1)*fact(J+j1-j2)*fact(J-j1+j2)*fact(j1+j2-J)/fact(j1+j2+J+1));
		b = sqrt(fact(J+M)*fact(J-M)*fact(j1+m1)*fact(j1-m1)*fact(j2+m2)*fact(j2-m2));
		double upper = std::min(std::min((j1+j2-J),(j1-m1)),(j2+m2));
        double lower = std::max(std::max((j2-J-m1), 0), (j1+m2-J));
		for(int k=lower; k<= upper; ++k){
			c1 = j1+j2-J-k;
			c2 = j1-m1-k;
			c3 = j2+m2-k;
			c4 = J-j2+m1+k;
			c5 = J-j1-m2+k;
			c += (pow(-1,k)/(fact(k)*fact(c1)*fact(c2)*fact(c3)*fact(c4)*fact(c5)));			
		} 
		return a*b*c;
	}	
}

double C(int s, int l_s, double l, double mp, double m_mp){
	int m = m_mp + mp;
	assert(l_s >= 0 && s >= 0 && l-s == l_s);
	if (mp < -s || mp > s ||  m_mp > l_s || m_mp < -l_s) 
	{	
		//std::cout << "!!!" << std::endl;	
		return 0;
	}
	else {
		double a = fact(2*(l_s))*fact(2*s)*fact(l+m)*fact(l-m);
		double b = fact(2*l)*fact(s+mp)*fact(s-mp)*fact(l-s+m-mp)*fact(l-s-m+mp);
		//std::cout << a << ";" << b << std::endl;
		if (b == 0) 
			return 0;
		else 
			return sqrt(a/b);
	}
}

double vecSquare(double v[]){
	return v[0]*v[0] + v[1]*v[1] + v[2]*v[2]; 
}

double getLen(double v[]){
	return sqrt(vecSquare(v));
}

double getTheta(double v[]){
	if (vecSquare(v) == 0) 
	{ 
		return 0.0; 
	}
	return acos(v[2]/sqrt(vecSquare(v)));
}

double getPhi(double v[]){
	//double alpha = atan(v[1]/v[0]);
	//if (asin(alpha)>=PI*0.5 && asin(alpha)<=PI):
	//	return PI + atan(v[1]/v[0]);
	//else:
		return atan2(v[1],v[0]);
}

double dotProduct(double a[], double b[]){
	double sum = 0;
	for (int i = 0; i < 3; i++)
		sum += a[i]*b[i];
	return sum;
}

double * vecSubstr(double a[], double b[]){
	double * c = new double[3];
	for (int i = 0; i < 3; i++)
		c[i] = a[i]-b[i];
	return c;
	
}


std::complex<double> Y(double rA[], unsigned l = l, int m = m){
	return boost::math::spherical_harmonic(l, m, getTheta(rA), getPhi(rA));
}

std::complex<double> G(double rA[], unsigned l = l, int m = m){
	return sqrt(4*PI/fact(2*l+1))*pow(getLen(rA),l)*Y(rA, l, m);	//POW XYINYA
}

std::complex<double> G_new(double rA[],double rB[], unsigned l = l, int m = m){	
	std::complex<double> sum = 0;
	double * rAB = vecSubstr(rA,rB);
	for (unsigned s = 0; s <=l; s++)
		for (int v = -s; v <= (int)s; v++)
			sum += C(s,l-s,l,v,m-v) * G(rB,s,v) * G(rAB,l-s, m-v);
	delete [] rAB;
	return sum;
}

double gaussian(double rA, double alpha){	
	return exp(-alpha*rA*rA);
}

double sph_bessel_mod(unsigned  l, double x){
	if ((l==0)&&(x==0))
		return 1;
	else if(x == 0)
		return 0;
	else
	return sqrt(PI/(2*x)) * boost::math::cyl_bessel_i(l + 0.5, x);
}

double gaussian_new(double rA[], double rAB[], double alpha){
	const double NormR = getLen(rA)*getLen(rA) - 2*dotProduct(rA,rAB);
	const double Norm = 4*PI*gaussian(NormR,alpha);
	const double lenRB = getLen(rA)*getLen(rA) - 2*dotProduct(rA,rAB) - getLen(rAB)*getLen(rAB);
	std::complex<double> sum =0;
	for (unsigned lp = 0; (int)lp <= infty; lp++)
		for(int mp = -lp; mp <= (int)lp; mp++)
			sum += pow(-1,lp) * sph_bessel_mod(lp, 2*alpha*getLen(rAB)*lenRB) * Y(rB, lp,mp) * pow(-1,mp) * Y(rAB, lp, -mp);
	return Norm * sum.real();
}

double AoOld(double rA[],double alpha,  unsigned l = l, int m = m){
	return pow((4*PI/fact(2*l+1)),-0.5)*G(rA,l,m).real()*gaussian(getLen(rA), alpha);
}

double AoNew(double rA[], double alpha, double rB[] = rB, unsigned l = l, int m = m){
	//rB = rA;
	double * rAB = vecSubstr(rA,rB);
	double R = getLen(rAB);
	const double NormR = getLen(rB)*getLen(rB) + R*R;
	const double Norm = 4*PI*exp(-NormR*alpha);
	std::cout << "----------" << std::endl;
	std::complex<double> sum = 0;
	for (unsigned s = 0; s <= l; s++)
		for (unsigned lp = 0; (int)lp <= infty; lp++)
			for (int v = -s; v <= (int)s; v++)
				for (int mp = -lp; mp <= (int)lp; mp++)
				{
					if (abs(m - v) > abs((int)l-(int)s)) { continue; } 
					std::complex<double> tmp = pow(-1,lp) * C(s,l-s,l,v,m-v)* 
						   pow(((4*PI*fact(2*l+1))/(fact(2*s+1)*fact(2*l-2*s+1))),0.5) *
						   pow(R,l-s) * sph_bessel_mod(lp,2*alpha*R*getLen(rB)) *
						   pow(getLen(rB),s) * Y(rB,s,v) * Y(rB,lp,mp) * 
						   Y(rAB,l-s,m-v) * pow(-1,mp) * Y(rAB,lp,-mp);
					sum += tmp;
					if (l - s == 0 && lp == 0) { std::cout << s << "; " << lp << "; " << v << "; " << mp << "-> " << tmp << std::endl; }
				}
	delete [] rAB;
	return Norm*sum.real();	
}


double AoNewDecomp(double rA[], double alpha, double rB[] = rB, unsigned l = l, int m = m){
	//rB = rA;
	double * rAB = vecSubstr(rA,rB);
	double R = getLen(rAB);
	const double NormR = getLen(rB)*getLen(rB) + R*R;
	const double Norm = 4*PI*exp(-NormR*alpha);
	std::complex<double> sum = 0;
	for (unsigned s = 0; s <= l; s++)
		for (unsigned lp = 0; (int)lp <= infty; lp++)
			for (int v = -s; v <= (int)s; v++)
				for (int mp = -lp; mp <= (int)lp; mp++)
					for(unsigned k = abs((int)s-(int)lp); k <= (s+lp); k++){
						//std::cout << s << " " << v << " " << lp << " " << mp << " " << k << std::endl;
						std::complex<double> tmp = pow(-1,lp)*C(s,l-s,l,v,m-v)*
							   pow(((4*PI*fact(2*l+1))/(fact(2*s+1)*fact(2*l-2*s+1))),0.5)*
							   pow(R,l-s)*sph_bessel_mod(lp,2*alpha*R*getLen(rB))*
							   pow(getLen(rB),s)*Y(rAB,l-s,m-v)*pow(-1,mp)*Y(rAB,lp,-mp)*
							   sqrt((2*s+1)*(2*lp+1)/(4*PI))*
							   1.0/sqrt(2*k+1)*CG(s,v,lp,mp,k,v+mp)*CG(s,0,lp,0,k,0)*Y(rB,k,v+mp);
						sum += tmp;
				//	if (l - s == 0 && lp == 0) { std::cout << s << "; " << lp << "; " << v << "; " << mp << "-> " << tmp << std::endl; }
					}
	delete [] rAB;
	return Norm*sum.real();	
}


int main(){
	//std::complex<double> Y = Y(rB);
	//std::cout << sph_bessel_mod(0,0) << std::endl;
	//std::cout << pow(-1, 4) << std::endl;
	//for (int i =0; i!=10; ++i)
	//	std::cout << i+1 <<") Ra : " << AoOld(rA, alpha) << " Rb: " << AoNew(rA,alpha) << std::endl;
	//std::cout << vecSubstr(rA,rB);
	
	std::cout << pow(0, 0) <<") Ra : " << AoOld(rA, alpha) << " Rb: " << AoNewDecomp(rA,alpha) << std::endl;
	/*
	std::cout << "----------" << std::endl;
	double * R = vecSubstr(rA,rA);
	std::cout << Y(R, 0, 0) << std::endl;
	std::cout << Y(rA, 0, 0) << std::endl;
	std::cout << Y(rB, 0, 0) << std::endl;
	
	
	std::cout << "----------" << std::endl;
	std::cout << CG(3,-1,2,-1,4,-2) << " =? " << C(3,2,5,-1,0)<< std::endl;

	delete [] R;
	*/
	return 0;
	
	
}
