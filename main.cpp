#include<iostream>
#include<fstream>
#include<cstring>
#include<sstream> 
#include<vector>
#include<locale>
#include<complex>
#include<cmath>
#include<iomanip>
#include<map>
#include<ctime>
#include<gsl/gsl_sf_legendre.h>
#include<gsl/gsl_sf_bessel.h>
#include <gsl/gsl_integration.h>

#include "stdio.h"


void printTimeStamp(const clock_t& tStart, const clock_t& tLocalStart){
	const clock_t tEnd=clock();
	std::cout<<"\n                         Block duration: "<<double(tEnd - tLocalStart) / CLOCKS_PER_SEC<<"s";
	std::cout<<"\n                           All duration: "<<double(tEnd - tStart) / CLOCKS_PER_SEC<<"s\n";
}

std::vector<std::string> parseWrds(const std::string& str){
	std::vector<std::string> res;
	std::string tStr;
	res.clear();
	std::stringstream inp(str);
	while (inp>>tStr) res.push_back(tStr);
	return res;
}

double doubleFactorial (const int& i){
	if (i<-1) std::cout<<"WARNING! DOUBLE FACTORIAL FOR VALUE "<<i<<" ARE WRONG CALCULATED\n";
	double res(1);
	for (int j=i; j>1; j-=2) res*=j;
	return res;
}

double binomialCoefficient (const int& n, const int& i){
	if ((n>20)||(n<0)||(i<0)||(i>n)) std::cout<<"WARNING! BINOMIAL COEFFICIENT MAY BE CALCULATED WITH ERROR\n";
	double res(1);
	for (int j=1; j<=i; j++){ res*=(n-j+1); res/=j;}
	return res;
}

double overlap_I1_1D(const double& x1, const double& alpha1, const int& n1, 
                     const double& x2, const double& alpha2, const int& n2){
	const double gamma = alpha1 + alpha2;
	const double P = ( alpha1 * x1 + alpha2 * x2 ) / gamma ;
	double res=0;
	for (int i1=0; i1<=n1; i1++)
		for (int i2=0; i2<=n2; i2++)
			if ((i1+i2)%2==0)
				res+=binomialCoefficient(n1,i1)*binomialCoefficient(n2,i2)*doubleFactorial(i1+i2-1)*
					pow(P-x1,n1-i1)*pow(P-x2,n2-i2)/pow(2*gamma,(i1+i2)/2);
	res *= sqrt(acos(0)*2.0/gamma)*exp(-alpha1*alpha2/gamma*(x1-x2)*(x1-x2));
	return res;
}

double moment1_1D(const double& x1, const double& alpha1, const int& n1, 
                     const double& x2, const double& alpha2, const int& n2){
	const double gamma = alpha1 + alpha2;
	const double P = ( alpha1 * x1 + alpha2 * x2 ) / gamma ;
	double res=0;
	for (int i1=0; i1<=n1; i1++)
		for (int i2=0; i2<=n2; i2++)
			if ((i1+i2)%2==0){
				res+=P*binomialCoefficient(n1,i1)*binomialCoefficient(n2,i2)*doubleFactorial(i1+i2-1)*
					pow(P-x1,n1-i1)*pow(P-x2,n2-i2)/pow(2*gamma,(i1+i2)/2);
			} else {
				res+=binomialCoefficient(n1,i1)*binomialCoefficient(n2,i2)*doubleFactorial(i1+i2)*
					pow(P-x1,n1-i1)*pow(P-x2,n2-i2)/pow(2*gamma,(i1+i2+1)/2);
			}
	res *= sqrt(acos(0)*2.0/gamma)*exp(-alpha1*alpha2/gamma*(x1-x2)*(x1-x2));
	return res;
}


/*
 * calc overlap integral over all 3D space
 * \int\int\int dx dy dz (x-x1)^nx1 (y-y1)^ny1 (z-z1)^nz1*exp(-alpha1*(r-r1)^2) * 
 *                       (x-x2)^nx2 (y-y2)^ny2 (z-z2)^nz2*exp(-alpha2*(r-r2)^2)
 */
void moments1_3D(const double& x1, const double& y1, const double& z1, const double& alpha1,
                   const int& nx1, const int& ny1, const int& nz1,
                   const double& x2, const double& y2, const double& z2, const double& alpha2,
                   const int& nx2, const int& ny2, const int& nz2,
		   double& mX, double& mY, double& mZ){
	double sX = overlap_I1_1D (x1, alpha1, nx1, x2, alpha2, nx2);
	double sY = overlap_I1_1D (y1, alpha1, ny1, y2, alpha2, ny2);
	double sZ = overlap_I1_1D (z1, alpha1, nz1, z2, alpha2, nz2);
	double m1X = moment1_1D (x1, alpha1, nx1, x2, alpha2, nx2);
	double m1Y = moment1_1D (y1, alpha1, ny1, y2, alpha2, ny2);
	double m1Z = moment1_1D (z1, alpha1, nz1, z2, alpha2, nz2);
	mX=m1X*sY*sZ;
	mY=m1Y*sX*sZ;
	mZ=m1Z*sX*sY;
	return ;
}

double overlap_I1_3D(const double& x1, const double& y1, const double& z1, const double& alpha1,
                     const int& nx1, const int& ny1, const int& nz1,
                     const double& x2, const double& y2, const double& z2, const double& alpha2,
                     const int& nx2, const int& ny2, const int& nz2){
	double res = overlap_I1_1D (x1, alpha1, nx1, x2, alpha2, nx2);
	res *= overlap_I1_1D (y1, alpha1, ny1, y2, alpha2, ny2);
	res *= overlap_I1_1D (z1, alpha1, nz1, z2, alpha2, nz2);
	return res;
}

class GeomAtom{
public:
	double x, y, z;
	std::string label;
	void scale(const double& f){ x *= f; y *= f; z *= f; }
	GeomAtom(): x(0), y(0), z(0), label(""){}
private:
};

class DeCartFunc{
public:
	DeCartFunc() : idXYZ(-1), idAlphaC(-1), nx(-1), ny(-1), nz(-1), c(0) {}
	void set (const int& idXYZ_, const int& idAlphaC_, const int& nx_, const int& ny_, const int& nz_) {
		idXYZ=idXYZ_;
		idAlphaC=idAlphaC_;
		nx=nx_;
		ny=ny_;
		nz=nz_;
	}
	void setC(const double& c_){ c=c_;}
	int idXYZ, idAlphaC, nx, ny, nz;
	double c;
};


class OneBF{
public:
	enum SHELL { undef, S, P, D, F};
	SHELL shell;
	static SHELL determineShell(const std::string& str){
		if (str=="S") return S;
		if (str=="P") return P;
		if (str=="D") return D;
		if (str=="F") return F;
		return undef;
	}
	std::string strShell() const{
		if (shell==S) return "S";
		if (shell==P) return "P";
		if (shell==D) return "D";
		if (shell==F) return "F";
		return "undef";
	}
	std::vector<std::pair<double, double> > alpha_C; // TODO затем перенести в приватную часть
	OneBF(): shell(undef), alpha_C(0){}
	void clear(){ shell = undef; alpha_C.clear(); }
	void add(const double& alpha, const double& C){ alpha_C.push_back(std::make_pair(alpha,C) ); }
};

class AbInitData{
public:
	void setTime(const clock_t& tStart_){tStart=tStart_;}
	enum GEOMUNITS { undef, bohr, angs };
	AbInitData():isBasis(false),isGeom(false),isOrbital(false),geomUnit(undef),geom(0),basis(0),xyzBasis(0),tStart(0){}
	bool loadGeom(char* filename){
		std::string tStr;
		GeomAtom tAtom;
		std::ifstream inp(filename);
		inp >> tStr;
		{
			std::locale loc;
			for (std::string::size_type i=0; i<tStr.length(); ++i)
				tStr[i] = std::tolower(tStr[i],loc);
		}
		if ( ( tStr == "bohr" ) || ( tStr == "b" ) )    geomUnit = bohr;
		if ( ( tStr == "angs" ) || ( tStr == "a" ) || ( tStr == "angstrom" ) ) geomUnit = angs;

		if ( geomUnit == undef ) {
			std::cout << "Error! \""<<tStr<<"\" type is not recognized as unit definition\n Geometry not accepted!\n";
			return false;
		}
		double factor(1);
		if ( geomUnit == angs ) factor=0.529177;

		while (	inp >> tAtom.label >> tAtom.x >> tAtom.y >> tAtom.z ){
			tAtom.scale(factor);
			geom.push_back(tAtom);
		}
		inp.close();
		return isGeom = true;
	}
	bool loadBasis(char* filename){
		std::string tStr;
		if (!isGeom){
			std::cout<<"Error! Information about geometryst be loaded before orbitals\n";
			return false;
		}
		std::vector<OneBF> atomBasis;
		atomBasis.clear();
		OneBF oneBF;
		std::string atomName;

		std::ifstream inp(filename);

		enum MODES { start, newBF, readBF}; // Я думаю что тут уже всем понятно что автора вставило от нумерованных констант
		MODES modes = start;
		int posLine=0;
		while (std::getline(inp,tStr))
		{
			std::string tStrIni(tStr);
			size_t posRep = tStr.find("(");
			if (posRep != std::string::npos) tStr=tStr.replace(posRep, 1, " ");
			posRep = tStr.find(")");
			if (posRep != std::string::npos) tStr=tStr.replace(posRep, 1, " ");
			posLine++;
			std::vector<std::string> parsed = parseWrds(tStr);
			if ((modes==start)&&(parsed.size()!=1)) continue;
			if ((modes==start)&&(parsed.size()==1)){ // начинаем читать первый элемент
				atomName=parsed[0];
				modes=newBF;
			       	continue;
			}
			if ((modes!=start)&&(parsed.size()==1)){ // переходим к чтению нового элемента
				basis.push_back(std::make_pair(atomName, atomBasis));
				atomBasis.clear();
				oneBF.clear();
				atomName=parsed[0];
				modes=newBF;
			       	continue;
			}
			if ((modes==newBF)&&(parsed.size()==0)){ // переходим к чтению нового элемента
				continue;
			}
			if ((modes==newBF)&&(parsed.size()>5)){ // переходим к чтению новой базисной функции
				oneBF.clear();
				oneBF.shell = OneBF::determineShell(parsed[1]);
				if (oneBF.shell == OneBF::undef) {
					std::cout<<"Error! Type shell \""<<parsed[1]<<"\" in string #"<<posLine<<":\n>>>"<<tStrIni<<"<<<\n in basis file is not recognized !\n";
					return false;
				}
				std::string::size_type sz;
				double alpha = std::stod (parsed[3],&sz);
				sz=0;
				double C = std::stod (parsed[5],&sz);
				oneBF.add(alpha,C);
				modes=readBF;
				continue;
			}
			if ((modes==readBF)&&(parsed.size()>5)){ // продолжаем читать базисную функцию
				if (oneBF.shell != OneBF::determineShell(parsed[1])) {
					std::cout<<"Error! Type shell \""<<parsed[1]<<"\" in string #"<<posLine<<":\n>>>"<<tStrIni<<"<<<\n in basis file is not matches with previos shell in this function!\n";
					return false;
				}
				std::string::size_type sz;
				double alpha = std::stod (parsed[3],&sz);
				sz=0;
				double C = std::stod (parsed[5],&sz);
				oneBF.add(alpha,C);
				continue;
			}
			if ((modes==readBF)&&(parsed.size()==0)){ // перестаем читать базисную функцию (мне нравятся мои комментарии, особенно их разнообразие!)
				atomBasis.push_back(oneBF);
				oneBF.clear();
				modes=newBF;
				continue;
			}
			std::cout<<"Error! String #"<<posLine<<":\n>>>"<<tStrIni<<"<<<\n in basis file is not recognized!\n";
			return false;
		}
		if (modes==readBF) atomBasis.push_back(oneBF);
		basis.push_back(std::make_pair(atomName, atomBasis));
		inp.close();
		if (basis.size()!=geom.size()){
			std::cout<<"Error! Number of basis centers("<<basis.size()<<") is not equal number of geometry centers ("<<geom.size()<<")!\n";
			return false;
		}
		renormInitialBasis();
		setupXyzBasis();
		return isBasis = true;	
	}
	bool calcMassCenter(double& xC, double& yC, double& zC, const char* filename) const{
		xC = yC = zC = 0;
		std::cout<<"\n ****************** START MASS CENTER CALCULATION ********** \n";
		if (!isGeom) {
			std::cout<<"Error! Isn't possible to calculate CM coordinates without loaded geometry!\n";
			return false;
		}
		std::map<std::string, double> mass;
		mass.clear();
		std::string tStr;
		double tMass;
		std::ifstream inp(filename);
		while (inp>>tStr>>tMass){
			if (mass.find(tStr)!=mass.end()){
				std::cout<<"Error! In mass file "<<filename<<" element "<<tStr<<" is present twice!\n";
				inp.close();
				return false;
			}
			mass[tStr]=tMass;
			std::cout<<"   For element "<<tStr<<" mass "<<tMass<<" was detemined\n";
		}
		inp.close();
		double m(0);
		for (int i=0; i<(int)geom.size(); i++){
			if (mass.find(geom[i].label)==mass.end()){
				std::cout<<"Error! For element "<<geom[i].label<<" mass is not found!\n";
				return false;
			}
			m += mass[geom[i].label];
			xC += mass[geom[i].label] * geom[i].x;
			yC += mass[geom[i].label] * geom[i].y;
			zC += mass[geom[i].label] * geom[i].z;
		}
		xC /= m;
		yC /= m;
		zC /= m;
		std::cout<<"   Coordinates for center mass (Bohr) are: "<<xC<<' '<<yC<<' '<<zC<<'\n';
		std::cout<<" ****************** END MASS CENTER CALCULATION ************ \n";
		return true;
	}
	bool calcOrbitalCenter(double& xC, double& yC, double& zC) const{
		clock_t tLocalStart = clock();
		std::cout<<"\n ****************** START ORBITAL CENTER CALCULATION ******* \n";
		xC = yC = zC =0;
		
		for (int i1=0; i1<(int)xyzBasis.size(); i1++)
			for (int i2=0; i2<(int)xyzBasis.size(); i2++){ // Можно конечно сократить вычисления в 2 раза, а смысл?
				double x1 = geom[xyzBasis[i1].idXYZ].x;
				double y1 = geom[xyzBasis[i1].idXYZ].y;
				double z1 = geom[xyzBasis[i1].idXYZ].z;
				double x2 = geom[xyzBasis[i2].idXYZ].x;
				double y2 = geom[xyzBasis[i2].idXYZ].y;
				double z2 = geom[xyzBasis[i2].idXYZ].z;
				int nx1 = xyzBasis[i1].nx;
				int ny1 = xyzBasis[i1].ny;
				int nz1 = xyzBasis[i1].nz;
				int nx2 = xyzBasis[i2].nx;
				int ny2 = xyzBasis[i2].ny;
				int nz2 = xyzBasis[i2].nz;
				std::vector<std::pair<double, double> > aC1=basis[xyzBasis[i1].idXYZ].second[xyzBasis[i1].idAlphaC].alpha_C;
				std::vector<std::pair<double, double> > aC2=basis[xyzBasis[i2].idXYZ].second[xyzBasis[i2].idAlphaC].alpha_C;
				double partMX(0);
				double partMY(0);
				double partMZ(0);
				for (int j1=0; j1<(int)aC1.size(); j1++)
					for (int j2=0; j2<(int)aC2.size(); j2++)
					{
						double mX, mY, mZ;
						moments1_3D(x1,y1,z1,aC1[j1].first,nx1,ny1,nz1,
						            x2,y2,z2,aC2[j2].first,nx2,ny2,nz2,
							    mX,mY,mZ);
						double I2=overlap_I1_3D(x1,y1,z1,aC1[j1].first,nx1,ny1,nz1,
						                        x1,y1,z1,aC1[j1].first,nx1,ny1,nz1);
						double I3=overlap_I1_3D(x2,y2,z2,aC2[j2].first,nx2,ny2,nz2,
						                        x2,y2,z2,aC2[j2].first,nx2,ny2,nz2);

						partMX += mX/sqrt(I2*I3)*aC1[j1].second*aC2[j2].second;
						partMY += mY/sqrt(I2*I3)*aC1[j1].second*aC2[j2].second;
						partMZ += mZ/sqrt(I2*I3)*aC1[j1].second*aC2[j2].second;
					}
				xC +=partMX*xyzBasis[i1].c*xyzBasis[i2].c;
				yC +=partMY*xyzBasis[i1].c*xyzBasis[i2].c;
				zC +=partMZ*xyzBasis[i1].c*xyzBasis[i2].c;
		}
		std::cout<<"   Coordinates for orbital mass (Bohr) are: "<<xC<<' '<<yC<<' '<<zC<<'\n';
		printTimeStamp(tStart,tLocalStart);
		std::cout<<" ****************** END ORBITAL CENTER CALCULATION ********* \n";
		return true;
	}

	void printDebug(){
		if (basis.size()!=geom.size()){
			std::cout<<"Error! Number of basis centers("<<basis.size()<<") is not equal number of geometry centers ("<<geom.size()<<")!\n";
			return;
		}

		for (int i=0; i<(int)basis.size(); i++){
			std::cout<<geom[i].label<<' '<<geom[i].x<<' '<<geom[i].y<<' '<<geom[i].z<<'\n';
			std::cout<<basis[i].first<<'\n';
			for (int j=0; j<(int)basis[i].second.size(); j++){
				std::cout<<"  "<<basis[i].second[j].strShell()<<'\n';
				for (int k=0; k<(int)basis[i].second[j].alpha_C.size(); k++){
					std::cout<<"     "<<basis[i].second[j].alpha_C[k].first<<' ';
					std::cout<<"     "<<basis[i].second[j].alpha_C[k].second<<'\n';
				}
			}
		}
		std::cout<<xyzBasis.size()<<"\n---------------------\n";
		for (int i=0; i<(int)xyzBasis.size(); i++){
			std::cout<<basis[xyzBasis[i].idXYZ].first<<' ';
			int nx=xyzBasis[i].nx;
			int ny=xyzBasis[i].ny;
			int nz=xyzBasis[i].nz;
			if ((nx+ny+nz)==0) std::cout<<"S";
			for (int j=0; j<nx; j++) std::cout<<"X";
			for (int j=0; j<ny; j++) std::cout<<"Y";
			for (int j=0; j<nz; j++) std::cout<<"Z";

			std::cout<<' '<<xyzBasis[i].c<<'\n';

		}
	}
	bool loadOrbital(char* filename){
		std::string tStr;
		if (!isBasis){
			std::cout<<"Error! Information about basis must be loaded before orbitals\n";
			return false;
		}
		std::vector<double> candidates;
		candidates.clear();
		std::ifstream inp(filename);
		while (std::getline(inp,tStr))
		{
			std::vector<std::string> parsed = parseWrds(tStr);
			std::string::size_type sz(0);
			candidates.push_back(std::stod (parsed[2],&sz));
		}
		inp.close();
		if (candidates.size() != xyzBasis.size()){
			std::cout<<"Error! Number of xyz primitives in basis ("<<xyzBasis.size()<<") is not equal number of orbitals coefficients ("<<candidates.size()<<")!\n";
			return false;
		}
		for (int i=0; i<(int)candidates.size(); i++)
			xyzBasis[i].setC(candidates[i]);
		renormOrbital();
		return isOrbital = true;
	}
	double getOrbitalValue(const double& x, const double& y, const double& z) const{
		double res(0);
		for (int i=0; i<(int)xyzBasis.size(); i++){
			double xC = geom[xyzBasis[i].idXYZ].x;
			double yC = geom[xyzBasis[i].idXYZ].y;
			double zC = geom[xyzBasis[i].idXYZ].z;
			double dx=x-xC;
			double dy=y-yC;
			double dz=z-zC;
			double r2=dx*dx+dy*dy+dz*dz;
			int nx = xyzBasis[i].nx;
			int ny = xyzBasis[i].ny;
			int nz = xyzBasis[i].nz;
			std::vector<std::pair<double, double> > aC=basis[xyzBasis[i].idXYZ].second[xyzBasis[i].idAlphaC].alpha_C;
			double partRes(0);
			for (int j=0; j<(int)aC.size(); j++){
				double S=overlap_I1_3D(xC,yC,zC,aC[j].first,nx,ny,nz,
				                        xC,yC,zC,aC[j].first,nx,ny,nz);
				partRes +=exp(-aC[j].first*r2)/sqrt(S)*aC[j].second;
			}
			res+=partRes*xyzBasis[i].c*pow(dx,nx)*pow(dy,ny)*pow(dz,nz);
		}
		return res;
	}

	bool integrate3Dcube(const double& xl, const double& xr, const double& yl, const double& yr,
	                     const double& zl, const double& zr, const double& d) const{
		clock_t tLocalStart = clock();
		std::cout<<"\n ****************** RUN 3D CUBIC INTEGRATION *************** \n";
		std::cout<<"      x = "<<xl<<".."<<xr<<'\n';
		std::cout<<"      y = "<<yl<<".."<<yr<<'\n';
		std::cout<<"      z = "<<zl<<".."<<zr<<'\n';
		std::cout<<"   step = "<<d<<'\n';

		double numS(0);
		double mX(0);
		double mY(0);
		double mZ(0);
		for (double x=xl; x<=xr; x+=d)
			for (double y=yl; y<=yr; y+=d)
				for (double z=zl; z<=zr; z+=d){
					double V2 = getOrbitalValue(x,y,z);
					V2*=V2;
					numS+=V2;
					mX+=V2*x;
					mY+=V2*y;
					mZ+=V2*z;
				}
		std::cout<<"   norm = "<<numS*d*d*d<<'\n';
		std::cout<<"   orbital center (bohr) : "<<mX*d*d*d<<' '<<mY*d*d*d<<' '<<mZ*d*d*d<<'\n';
		printTimeStamp(tStart,tLocalStart);
		std::cout<<" ****************** END 3D CUBIC INTEGRATION *************** \n";
		return true;
	}
	 
private:
	bool isBasis, isGeom, isOrbital;
	// Геометрия
	GEOMUNITS geomUnit;
public:
	std::vector<GeomAtom> geom;
	// Базис
	std::vector<std::pair<std::string,std::vector<OneBF> > > basis;
	std::vector<DeCartFunc> xyzBasis;
private:
	clock_t tStart;
//	Перенормировка исходного базиса
	void renormInitialBasis(){
		std::cout<<" ****************** START RENORMALIZATION BASIS ************ \n";
		for (int i=0; i<(int)basis.size(); i++)
			for (int j=0; j<(int)basis[i].second.size(); j++){
				int nx=-1;  // FIXME Дальше хрень от лентяйства. Сделать метод в OneBF!!!!!
					// НЕТ, не делать метод в OneBF пока не ясна ситуация с L оболочкой!
					// Сосчитать в FF что-нить в 6-31G и проверить выдачу базиса для L,
					// вдруг проблема отпадет сама собой.
				if (basis[i].second[j].shell == OneBF::S ) nx=0;
				if (basis[i].second[j].shell == OneBF::P ) nx=1;
				if (basis[i].second[j].shell == OneBF::D ) nx=2;
				if (basis[i].second[j].shell == OneBF::F ) nx=3;
				if (nx==-1) {
					std::cout<<"Error! RENORM IS FALSE\n";
					return;
				}
			
				double S(0);
				for (int k1(0); k1<(int)basis[i].second[j].alpha_C.size(); k1++)
					for (int k2(0); k2<(int)basis[i].second[j].alpha_C.size(); k2++){ // Ничерта непонятно?! Я старался :)
						double I1=overlap_I1_3D(0,0,0,basis[i].second[j].alpha_C[k1].first,nx,0,0,
						                        0,0,0,basis[i].second[j].alpha_C[k2].first,nx,0,0);
						double I2=overlap_I1_3D(0,0,0,basis[i].second[j].alpha_C[k1].first,nx,0,0,
						                        0,0,0,basis[i].second[j].alpha_C[k1].first,nx,0,0);
						double I3=overlap_I1_3D(0,0,0,basis[i].second[j].alpha_C[k2].first,nx,0,0,
						                        0,0,0,basis[i].second[j].alpha_C[k2].first,nx,0,0);
						S+=I1/sqrt(I2*I3)*basis[i].second[j].alpha_C[k1].second*basis[i].second[j].alpha_C[k2].second;
					}
				std::cout<<"   Function #"<<j+1<<' '<<basis[i].second[j].strShell()<<" for atom #"<<i+1<<' ';
				std::cout<<basis[i].first<<" renormalized by "<<1/sqrt(S)<<'\n';
				for (int k1(0); k1<(int)basis[i].second[j].alpha_C.size(); k1++)
					basis[i].second[j].alpha_C[k1].second /= sqrt(S);
			}
		std::cout<<" ****************** END RENORMALIZATION BASIS **************\n";
	}
	void renormOrbital() { // перенормировка орбитали
		clock_t tLocalStart = clock();
		std::cout<<"\n ****************** START RENORMALIZATION ORBITAL ********** \n";
		double S(0);
		for (int i1=0; i1<(int)xyzBasis.size(); i1++)
			for (int i2=0; i2<(int)xyzBasis.size(); i2++){ // Можно конечно сократить вычисления в 2 раза, а смысл?
				double x1 = geom[xyzBasis[i1].idXYZ].x;
				double y1 = geom[xyzBasis[i1].idXYZ].y;
				double z1 = geom[xyzBasis[i1].idXYZ].z;
				double x2 = geom[xyzBasis[i2].idXYZ].x;
				double y2 = geom[xyzBasis[i2].idXYZ].y;
				double z2 = geom[xyzBasis[i2].idXYZ].z;
				int nx1 = xyzBasis[i1].nx;
				int ny1 = xyzBasis[i1].ny;
				int nz1 = xyzBasis[i1].nz;
				int nx2 = xyzBasis[i2].nx;
				int ny2 = xyzBasis[i2].ny;
				int nz2 = xyzBasis[i2].nz;
				std::vector<std::pair<double, double> > aC1=basis[xyzBasis[i1].idXYZ].second[xyzBasis[i1].idAlphaC].alpha_C;
				std::vector<std::pair<double, double> > aC2=basis[xyzBasis[i2].idXYZ].second[xyzBasis[i2].idAlphaC].alpha_C;
				double partS(0);
				for (int j1=0; j1<(int)aC1.size(); j1++)
					for (int j2=0; j2<(int)aC2.size(); j2++)
					{
						double I1=overlap_I1_3D(x1,y1,z1,aC1[j1].first,nx1,ny1,nz1,
						                        x2,y2,z2,aC2[j2].first,nx2,ny2,nz2);
						double I2=overlap_I1_3D(x1,y1,z1,aC1[j1].first,nx1,ny1,nz1,
						                        x1,y1,z1,aC1[j1].first,nx1,ny1,nz1);
						double I3=overlap_I1_3D(x2,y2,z2,aC2[j2].first,nx2,ny2,nz2,
						                        x2,y2,z2,aC2[j2].first,nx2,ny2,nz2);
						partS += I1/sqrt(I2*I3)*aC1[j1].second*aC2[j2].second;
					}
				S+=partS*xyzBasis[i1].c*xyzBasis[i2].c;
		}
		for (int i=0; i<(int)xyzBasis.size(); i++) xyzBasis[i].c /= sqrt(S);
		std::cout<<" Orbital was renormed on factor "<<std::setprecision(10)<<1.0/sqrt(S)<<'\n';
		printTimeStamp(tStart,tLocalStart);
		std::cout<<" ****************** END RENORMALIZATION ORBITAL ************\n";
	}


	
//	Установление декартового базиса
	void setupXyzBasis(){
		DeCartFunc deCartFunc;
		xyzBasis.clear();
		for (int i=0; i<(int)basis.size(); i++)
			for (int j=0; j<(int)basis[i].second.size(); j++){
				if (basis[i].second[j].shell == OneBF::S ){
					deCartFunc.set(i,j,0,0,0); xyzBasis.push_back(deCartFunc);
				}
				if (basis[i].second[j].shell == OneBF::P ){
					deCartFunc.set(i,j,1,0,0); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,0,1,0); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,0,0,1); xyzBasis.push_back(deCartFunc);
				}
				if (basis[i].second[j].shell == OneBF::D ){
					deCartFunc.set(i,j,2,0,0); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,0,2,0); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,0,0,2); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,1,1,0); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,1,0,1); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,0,1,1); xyzBasis.push_back(deCartFunc);
				}
				if (basis[i].second[j].shell == OneBF::F ){
					deCartFunc.set(i,j,3,0,0); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,0,3,0); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,0,0,3); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,2,1,0); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,2,0,1); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,1,2,0); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,0,2,1); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,1,0,2); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,0,1,2); xyzBasis.push_back(deCartFunc);
					deCartFunc.set(i,j,1,1,1); xyzBasis.push_back(deCartFunc);
				}
			}
	}
};

class SmartXyzBasis{
public:
	SmartXyzBasis(): x(0),y(0),z(0),nx(0),ny(0),nz(0),aC(0){aC.clear();}
	double x,y,z;
	int nx,ny,nz;
	std::vector<std::pair<double,double> > aC;
};


class PackedXyzBasis{
public:
	PackedXyzBasis(): x(0),y(0),z(0),alpha(0),c{0},isS(false),isP(false),isD(false),isF(false){clear();} // А вдруг?!
private:
	double x,y,z,alpha,c[20];  //  0  s;  1  x;  2  y;  3   z;   4 x2;  5  y2;  6  z2;  7  xy;  8  xz;  9  yz
	bool isS,isP,isD,isF;      // 10 x3; 11 y3; 12 z3; 13 x2y; 14 x2z; 15 xy2; 16 y2z; 17 xz2; 18 yz2; 19 xyz
public:
	void clear(){ 
		x=y=z=alpha=0;
		for (int i=0; i<20; i++) c[i]=0;
		isS = isP = isD = isF = false;
	}
	int getNoBF() const{
		int res=0;
		if (isS) res +=  1;
		if (isP) res +=  3;
		if (isD) res +=  6;
		if (isF) res += 10;
		return res;
	}
	void getNoZero(double nx[20], double ny[20], double nz[20], double valC[20], int& n) const{ // n means "no Zero Elements"
		n=0;
		if ((isS)&&(c[ 0]!=0)){ nx[n]=0; ny[n]=0; nz[n]=0; valC[n]=c[ 0]; n++;}
		if ((isP)&&(c[ 1]!=0)){ nx[n]=1; ny[n]=0; nz[n]=0; valC[n]=c[ 1]; n++;}
		if ((isP)&&(c[ 2]!=0)){ nx[n]=0; ny[n]=1; nz[n]=0; valC[n]=c[ 2]; n++;}
		if ((isP)&&(c[ 3]!=0)){ nx[n]=0; ny[n]=0; nz[n]=1; valC[n]=c[ 3]; n++;}
		if ((isD)&&(c[ 4]!=0)){ nx[n]=2; ny[n]=0; nz[n]=0; valC[n]=c[ 4]; n++;}
		if ((isD)&&(c[ 5]!=0)){ nx[n]=0; ny[n]=2; nz[n]=0; valC[n]=c[ 5]; n++;}
		if ((isD)&&(c[ 6]!=0)){ nx[n]=0; ny[n]=0; nz[n]=2; valC[n]=c[ 6]; n++;}
		if ((isD)&&(c[ 7]!=0)){ nx[n]=1; ny[n]=1; nz[n]=0; valC[n]=c[ 7]; n++;}
		if ((isD)&&(c[ 8]!=0)){ nx[n]=1; ny[n]=0; nz[n]=1; valC[n]=c[ 8]; n++;}
		if ((isD)&&(c[ 9]!=0)){ nx[n]=0; ny[n]=1; nz[n]=1; valC[n]=c[ 9]; n++;}
		if ((isF)&&(c[10]!=0)){ nx[n]=3; ny[n]=0; nz[n]=0; valC[n]=c[10]; n++;}
		if ((isF)&&(c[11]!=0)){ nx[n]=0; ny[n]=3; nz[n]=0; valC[n]=c[11]; n++;}
		if ((isF)&&(c[12]!=0)){ nx[n]=0; ny[n]=0; nz[n]=3; valC[n]=c[12]; n++;}
		if ((isF)&&(c[13]!=0)){ nx[n]=2; ny[n]=1; nz[n]=0; valC[n]=c[13]; n++;}
		if ((isF)&&(c[14]!=0)){ nx[n]=2; ny[n]=0; nz[n]=1; valC[n]=c[14]; n++;}
		if ((isF)&&(c[15]!=0)){ nx[n]=1; ny[n]=2; nz[n]=0; valC[n]=c[15]; n++;}
		if ((isF)&&(c[16]!=0)){ nx[n]=0; ny[n]=2; nz[n]=1; valC[n]=c[16]; n++;}
		if ((isF)&&(c[17]!=0)){ nx[n]=1; ny[n]=0; nz[n]=2; valC[n]=c[17]; n++;}
		if ((isF)&&(c[18]!=0)){ nx[n]=0; ny[n]=1; nz[n]=2; valC[n]=c[18]; n++;}
		if ((isF)&&(c[19]!=0)){ nx[n]=1; ny[n]=1; nz[n]=1; valC[n]=c[19]; n++;}
	}
	void set(const double& x_, const double& y_, const double& z_, const double& a_) { // TODO когда-нибудь тут должны были бы возникнуть проверки
		x=x_; y=y_; z=z_; alpha=a_;}
	double getValue(const double& valueX, const double& valueY, const double& valueZ) const{
		const double dx = valueX - x;
		const double dy = valueY - y;
		const double dz = valueZ - z;

		const double dx2 = dx * dx;
		const double dy2 = dy * dy;
		const double dz2 = dz * dz;

		double r2 = dx2 + dy2 + dz2;
		
		double res(0);
		if (isS) res += c[0];
		if (isP) res += c[1] * dx + c[2] * dy + c[3] * dz; 
		if (isD) res += c[4]*dx2 + c[5]*dy2 + c[6]*dz2 + c[7]*dx*dy + c[8]*dx*dz + c[9]*dy*dz;
		if (isF) res += (c[10]*dx+c[13]*dy+c[14]*dz)*dx2 + (c[11]*dy+c[15]*dx+c[16]*dz)*dy2 + (c[12]*dz+c[17]*dx+c[18]*dy)*dz2 + c[19]*dx*dy*dz;
		res *= exp(-alpha*r2);
		return res;
	}
	void getXyzAlpha(double& x_, double& y_, double& z_, double& a_) const  { x_ = x; y_ = y; z_ = z; a_ = alpha; }

	bool addCbyNxNyNz(const int& nx, const int& ny, const int& nz, const double& C){
		if ( (nx+ny+nz) == 0 ) isS = true;
		if ( (nx+ny+nz) == 1 ) isP = true;
		if ( (nx+ny+nz) == 2 ) isD = true;
		if ( (nx+ny+nz) == 3 ) isF = true;
		if ( (nx==0)&&(ny==0)&&(nz==0) ) { c[ 0] += C; return true; }
		if ( (nx==1)&&(ny==0)&&(nz==0) ) { c[ 1] += C; return true; }
		if ( (nx==0)&&(ny==1)&&(nz==0) ) { c[ 2] += C; return true; }
		if ( (nx==0)&&(ny==0)&&(nz==1) ) { c[ 3] += C; return true; }
		if ( (nx==2)&&(ny==0)&&(nz==0) ) { c[ 4] += C; return true; }
		if ( (nx==0)&&(ny==2)&&(nz==0) ) { c[ 5] += C; return true; }
		if ( (nx==0)&&(ny==0)&&(nz==2) ) { c[ 6] += C; return true; }
		if ( (nx==1)&&(ny==1)&&(nz==0) ) { c[ 7] += C; return true; }
		if ( (nx==1)&&(ny==0)&&(nz==1) ) { c[ 8] += C; return true; }
		if ( (nx==0)&&(ny==1)&&(nz==1) ) { c[ 9] += C; return true; }
		if ( (nx==3)&&(ny==0)&&(nz==0) ) { c[10] += C; return true; }
		if ( (nx==0)&&(ny==3)&&(nz==0) ) { c[11] += C; return true; }
		if ( (nx==0)&&(ny==0)&&(nz==3) ) { c[12] += C; return true; }
		if ( (nx==2)&&(ny==1)&&(nz==0) ) { c[13] += C; return true; }
		if ( (nx==2)&&(ny==0)&&(nz==1) ) { c[14] += C; return true; }
		if ( (nx==1)&&(ny==2)&&(nz==0) ) { c[15] += C; return true; }
		if ( (nx==0)&&(ny==2)&&(nz==1) ) { c[16] += C; return true; }
		if ( (nx==1)&&(ny==0)&&(nz==2) ) { c[17] += C; return true; }
		if ( (nx==0)&&(ny==1)&&(nz==2) ) { c[18] += C; return true; }
		if ( (nx==1)&&(ny==1)&&(nz==1) ) { c[19] += C; return true; }
		return false;

	}
	bool isContain(const double& x_, const double& y_, const double& z_, const double& alpha_) const{
		return ( ( x == x_ ) && ( y == y_ ) && ( z == z_ ) && ( alpha == alpha_ ) );
	}
};

class SmartOrbital{ // поддерживаются только s,p,d и f функции в данной реализации
public:
	SmartOrbital(): xyzBasis(0),tStart(0){ xyzBasis.clear(); }
	void setTime(const clock_t& tStart_){tStart=tStart_;}
	bool setDeCartFromFF(AbInitData& ini){
		clock_t tLocalStart = clock();
		std::cout<<"\n ******* START INITIALIZATION FROM FIREFLY ORBITAL ***** \n";
		int nTotalFunc(0);
		for (int i=0; i<(int)ini.xyzBasis.size(); i++) nTotalFunc += ini.basis[ini.xyzBasis[i].idXYZ].second[ini.xyzBasis[i].idAlphaC].alpha_C.size();
		std::cout<<"   Initial FF orbital contain "<<nTotalFunc<<" functions grouped into "<<ini.xyzBasis.size()<<" groups.\n";

		xyzBasis.clear();
		for (int i=0; i<(int)ini.xyzBasis.size(); i++){
			double x = ini.geom[ini.xyzBasis[i].idXYZ].x;
			double y = ini.geom[ini.xyzBasis[i].idXYZ].y;
			double z = ini.geom[ini.xyzBasis[i].idXYZ].z;
			int nx = ini.xyzBasis[i].nx;
			int ny = ini.xyzBasis[i].ny;
			int nz = ini.xyzBasis[i].nz;
			for (int j=0; j<(int)ini.basis[ini.xyzBasis[i].idXYZ].second[ini.xyzBasis[i].idAlphaC].alpha_C.size(); j++){
				double alpha=ini.basis[ini.xyzBasis[i].idXYZ].second[ini.xyzBasis[i].idAlphaC].alpha_C[j].first;
				double S=overlap_I1_3D(x,y,z,alpha,nx,ny,nz,
				                       x,y,z,alpha,nx,ny,nz);
				double C=ini.basis[ini.xyzBasis[i].idXYZ].second[ini.xyzBasis[i].idAlphaC].alpha_C[j].second*ini.xyzBasis[i].c/sqrt(S);
				int pos(-1);
				for (int k=0; k<(int)xyzBasis.size(); k++)
					if (xyzBasis[k].isContain(x,y,z,alpha)) pos = k;
				if ( pos == -1) {
					PackedXyzBasis tBasis;
					tBasis.set(x,y,z,alpha);
					tBasis.addCbyNxNyNz(nx,ny,nz,C);
					xyzBasis.push_back(tBasis);
				} else {
					xyzBasis[pos].addCbyNxNyNz(nx,ny,nz,C);
				}
	
			}
		}
		nTotalFunc=0;
		for (int i=0; i<(int)xyzBasis.size(); i++) nTotalFunc += xyzBasis[i].getNoBF();
		std::cout<<"   New smart orbital contain not more than "<<nTotalFunc<<" functions grouped into "<<xyzBasis.size()<<" groups.\n";

		printTimeStamp(tStart,tLocalStart);
		std::cout<<" ******* END INITIALIZATION FROM FIREFLY ORBITAL ******* \n";
		return true;
	}

	bool calcOrbitalCenter(double& xC, double& yC, double& zC) const{
		clock_t tLocalStart = clock();
		std::cout<<"\n ******* START ORBITAL CENTER CALCULATION FOR SMART ORBITAL NEW ** \n";
		xC = yC = zC =0;
		for (int i1=0; i1<(int)xyzBasis.size(); i1++)
			for (int i2=0; i2<(int)xyzBasis.size(); i2++){	// Можно конечно сократить вычисления в 2 раза, а смысл?/
				double x1,y1,z1,alpha1,x2,y2,z2,alpha2;
				double nx1[20],ny1[20],nz1[20],c1[20];
				double nx2[20],ny2[20],nz2[20],c2[20];
				int n1,n2;
				xyzBasis[i1].getXyzAlpha(x1,y1,z1,alpha1);
				xyzBasis[i2].getXyzAlpha(x2,y2,z2,alpha2);
				xyzBasis[i1].getNoZero(nx1, ny1, nz1, c1, n1);
				xyzBasis[i2].getNoZero(nx2, ny2, nz2, c2, n2);
				for (int j1=0; j1<n1; j1++)
					for (int j2=0; j2<n2; j2++){
						double mX, mY, mZ;
						moments1_3D(x1, y1, z1, alpha1, nx1[j1], ny1[j1], nz1[j1],
						            x2, y2, z2, alpha2, nx2[j2], ny2[j2], nz2[j2],
							    mX,mY,mZ);
						xC += mX*c1[j1]*c2[j2];
						yC += mY*c1[j1]*c2[j2];
						zC += mZ*c1[j1]*c2[j2];
					}
				}
		std::cout<<"   Coordinates for orbital mass (Bohr) are: "<<xC<<' '<<yC<<' '<<zC<<'\n';
		printTimeStamp(tStart,tLocalStart);
		std::cout<<" ******* END ORBITAL CENTER CALCULATION FOR SMART ORBITAL NEW *** \n";
		return true;
	}

	double getOrbitalValue(const double& x, const double& y, const double& z) const{
		double res(0);
		for (int i=0; i<(int)xyzBasis.size(); i++) res+=xyzBasis[i].getValue(x,y,z);
		return res;
	}

	bool integrate3Dcube(const double& xl, const double& xr, const double& yl, const double& yr,
	                     const double& zl, const double& zr, const double& d) const{
		clock_t tLocalStart = clock();
		std::cout<<"\n ************** RUN 3D CUBIC INTEGRATION IN NEW SMART ORB ** \n";
		std::cout<<"      x = "<<xl<<".."<<xr<<'\n';
		std::cout<<"      y = "<<yl<<".."<<yr<<'\n';
		std::cout<<"      z = "<<zl<<".."<<zr<<'\n';
		std::cout<<"   step = "<<d<<'\n';

		double numS(0);
		double mX(0);
		double mY(0);
		double mZ(0);
		for (double x=xl; x<=xr; x+=d)
			for (double y=yl; y<=yr; y+=d)
				for (double z=zl; z<=zr; z+=d){
					double V2 = getOrbitalValue(x,y,z);
					V2*=V2;
					numS+=V2;
					mX+=V2*x;
					mY+=V2*y;
					mZ+=V2*z;
				}
		std::cout<<"   norm = "<<numS*d*d*d<<'\n';
		std::cout<<"   orbital center (bohr) : "<<mX*d*d*d<<' '<<mY*d*d*d<<' '<<mZ*d*d*d<<'\n';
		printTimeStamp(tStart,tLocalStart);
		std::cout<<" ************** END 3D CUBIC INTEGRATION IN NEW SMART ORB ** \n";
		return true;
	}

	double* integrateSpheric(const double& xC, const double& yC, const double& zC,
            const int& maxL, const double& maxR, const double& dR, const double& dA) const{
		using namespace std::complex_literals;
		std::complex<double> i1(0,1);
        //clock_t tLocalStart = clock();
        std::complex<double>* sumLM = new std::complex<double> [(maxL+1) * (maxL+1)];
        double* integral = new double[maxL+1]();

        for (double r = 0; r <= maxR; r+=dR){
            for (int i=0; i< (maxL+1)*(maxL+1); i++) sumLM[i]=0;
            	for (double phi = 0; phi < 2*M_PI; phi += dA)
	                for(double theta = 0; theta < M_PI; theta += dA){
	                    double x = r * sin(theta) * cos(phi) + xC;
	                    double y = r * sin(theta) * sin(phi) + yC;
	                    double z = r * cos(theta) + zC;
	                    double v = getOrbitalValue(x, y, z);
	                    double result_array[gsl_sf_legendre_array_n(maxL)];
	                    gsl_sf_legendre_array_e(GSL_SF_LEGENDRE_SPHARM, maxL, cos(theta), -1, result_array);
	                    int pos=0;
	                    for (int L = 0; L <= maxL; L++)
	                        for (int M = -L; M <= L; M++){
	                        	if(M >= 0)
	                        		sumLM[pos] += v * exp(-phi*M*i1) * result_array[gsl_sf_legendre_array_index(L,abs(M))] * sin(theta) * dA * dA;//gsl_sf_legendre_array(L,M,theta,phi) * sin(theta);
	                        	else if(M < 0)
	    							sumLM[pos] += pow(-1,M) * v * exp(phi*M*i1) * result_array[gsl_sf_legendre_array_index(L,abs(M))] * sin(theta) * dA * dA;
	    						pos++;
	                        }
                }
            std::cout << r << std::endl;    
            int pos = 0;
            for(int L = 0; L <= maxL; L++){
            	for(int M = -L; M <= L; M++){
            		integral[L] += r*r*dR*(double)(sumLM[pos].real()*sumLM[pos].real() + sumLM[pos].imag()*sumLM[pos].imag());
            		pos++;
            	}
            	std::cout << integral[L] << std::endl;
            }
            //std::cout << sumLM[0] * r*r*dR << std::endl;
            //std::cout << integral[0] << std::endl;
        }
        delete[] sumLM;

		return integral;
	} 

	std::complex<double> F( const int& L, const int& M, const double& k,
			   const double& xC,   const double& yC, const double& zC,
			   const double& Rmax, const double& dR, const double& dA) const{

		using namespace std::complex_literals;
		std::complex<double> i1(0,1);

        //clock_t tLocalStart = clock();
        //std::complex<double>* sumLM = new std::complex<double> [(maxL+1) * (maxL+1)];
        std::complex<double> Rlm = std::complex<double>(0,0);
        std::complex<double> res = std::complex<double>(0,0);

        for (double r = 0; r <= Rmax; r+=dR){
            //for (int i=0; i< (maxL+1)*(maxL+1); i++) sumLM[i]=0;
            	for (double phi = 0; phi < 2*M_PI; phi += dA)
	                for(double theta = 0; theta < M_PI; theta += dA){
	                    double x = r * sin(theta) * cos(phi) + xC;
	                    double y = r * sin(theta) * sin(phi) + yC;
	                    double z = r * cos(theta) + zC;
	                    double v = getOrbitalValue(x, y, z);
	                    double result_array[gsl_sf_legendre_array_n(L)];
	                    gsl_sf_legendre_array_e(GSL_SF_LEGENDRE_SPHARM, L, cos(theta), -1, result_array);
	                    //int pos=0;
	                    //for (int L = 0; L <= maxL; L++)
	                    //   for (int M = -L; M <= L; M++){
	                        	if(M >= 0)
	                        		Rlm += v * exp(-phi*M*i1) * result_array[gsl_sf_legendre_array_index(L,abs(M))] * sin(theta) * dA * dA;//gsl_sf_legendre_array(L,M,theta,phi) * sin(theta);
	                        	else if(M < 0)
	    							Rlm += pow(-1,M) * v * exp(phi*M*i1) * result_array[gsl_sf_legendre_array_index(L,abs(M))] * sin(theta) * dA * dA;
	    				//		pos++;
	                    //    }
                }
            //std::cout << r << std::endl;    
            //int pos = 0;
            //for(int L = 0; L <= maxL; L++){
            //	for(int M = -L; M <= L; M++){
            		res += gsl_sf_bessel_jl(L,k*r)*r*r*r*dR*Rlm;
            		//std::cout << res << std::endl;
            //		pos++;
           	// 	}
            //	std::cout << integral[L] << std::endl;
            //}
            //std::cout << sumLM[0] * r*r*dR << std::endl;
            //std::cout << integral[0] << std::endl;
        }
        //delete[] sumLM;

		return pow(4*M_PI,1.5)*pow(3,-0.5)*res;
	}

	double beta( 	const int& Lmax, const double& k,
			   		const double& xC, const double& yC, const double& zC,
			   		const double& Rmax, const double& dR, const double& dA) const {
		std::complex<double> Fm = 0;
		std::complex<double> Fp = 0;

		// I = A + A*beta*B2
		std::complex<double> I = 0;
		std::complex<double> A = 0;

		for (int L = 0; L <= Lmax; L++)
	    	for (int M = -L; M <= L; M++){
	    		if ((L != 0)&&(abs(M) <= L-1))
	    			Fm = F(L-1,M,k,xC,yC,zC,Rmax,dR,dA);
	    		else
	    			Fm = 0;

	    		Fp = F(L+1,M,k,xC,yC,zC,Rmax,dR,dA);
	    		
				I += 1.0/((4*M_PI)*(4*M_PI)*pow(2*L+1,1.5))*(Fm*Fm*std::complex<double>(L*(L-1),0)+std::complex<double>(3,0)*(std::conj(Fm)*Fp+Fm*std::conj(Fp))*std::complex<double>(L*(L+1),0)+Fp*Fp*std::complex<double>((L+1)*(L+2)));
				A += (Fm*Fm*std::complex<double>(L,0)+Fp*Fp*std::complex<double>(L+1,0))*1.0/((4*M_PI)*(4*M_PI)*pow(2*L+1,0.5));

				printf("L = %2d  M = %2d  beta = %2.6lf  Fm = ",L,M,abs(I*1.0/A)); std::cout << Fm << "  Fp = " << Fp << "  I = " << I << std::endl;
	    }
		
		double res = abs(I*1.0/A);

		return res;
	}

	

private:
	std::vector<PackedXyzBasis> xyzBasis;
	clock_t tStart;
};

double f(double x, void * p)
  {
    double alpha = *(double *) params;
    double v = exp(-alpha*x*x);

    return  v;
  }

	double intergrator(){
		gsl_integration_workspace* w = gsl_integration_workspace_alloc (1000);

		double result, error;
		double expected = -4.0;
  		double alpha = 1.0;

		gsl_function F;
		F.function = &f;
		F.params = &alpha;


		gsl_integration_qagi(&F,0,1e-7,w,&result,&error);

		printf ("result          = % .18f\n", result);
	  	printf ("exact result    = % .18f\n", expected);
  		printf ("estimated error = % .18f\n", error);
  		printf ("actual error    = % .18f\n", result - expected);
  		printf ("intervals       = %zu\n", w->size);

  		gsl_integration_workspace_free (w);
		
		return 0;
	}

//int main(int argc, char* argv[]){
int main(){
	clock_t tStart = clock();
	AbInitData iniData;
	iniData.setTime(tStart);
	if ( !iniData.loadGeom    ((char*)"geom.txt" )   ) return 1;
	if ( !iniData.loadBasis   ((char*)"basis.txt")   ) return 1;
	if ( !iniData.loadOrbital ((char*)"orbital.txt") ) return 1;
	iniData.printDebug();
	double xC,yC,zC;
	
	if (!iniData.calcMassCenter(xC,yC,zC,(char*)"mass.txt")) return 1;


	SmartOrbital smartOrbitalNew; 
	smartOrbitalNew.setTime(tStart);
	smartOrbitalNew.setDeCartFromFF(iniData);

	smartOrbitalNew.calcOrbitalCenter(xC,yC,zC);
	smartOrbitalNew.integrate3Dcube(-10,25,-15,5,-15,10,0.8);
	smartOrbitalNew.integrate3Dcube(-10,25,-15,5,-15,10,0.4);
	//printf("spheric = %.5lf\n", smartOrbitalNew.integrateSpheric(0,0,0,1,1,0.1,2*M_PI/10));
	
	/*
	//int Lmax = 50;
	int Lmax = 10;
	//auto result = smartOrbitalNew.integrateSpheric(xC,yC,zC,Lmax,20,0.2,M_PI/300);
	auto result = smartOrbitalNew.integrateSpheric(xC,yC,zC,Lmax,10,1,M_PI/50);
	
	double sum = 0;
	for (int L = 0; L <= Lmax; L++)
	{
		std::cout << L << " --> " << result[L] << std::endl;
		sum += result[L];
	}
	std::cout << "Sum: " << sum << std::endl;
	
	delete[] result;
	*/

/*
	double x = 0.55;
	int Lmax = 6;
	double k = 1;
	//std::cout << "F: " << smartOrbitalNew.betaLM(L,M,k,xC,yC,zC,20,0.2,M_PI/300) << std::endl;

	double* beta = new double[100];

	int i = 0;
	for(double eV = 0.1; eV <= 2.1; eV+=0.1){
		k = pow(eV/27.2114*2,0.5);
		beta[i] = smartOrbitalNew.beta(Lmax,k,xC,yC,zC,20,0.5,M_PI/100);
		printf("%d step %2.3lf eV beta = %2.6lf\n",i,eV,beta[i]);
		i++;
	}

	for(int i = 0; i < 50; i++) printf("―");
    printf("\n");
    printf("%7s%15s%19s\n","k","eV","beta");
    for(int i = 0; i < 50; i++) printf("―");
    printf("\n"); 
    i = 0;  
	for(double eV = 0.1; eV <= 2.1; eV+=0.1){
		k = pow(eV/27.2114*2,0.5);
		printf("%9.3lf%15.3lf%19.6lf\n",k,eV,beta[i]);
		i++;
	}
	for(int i = 0; i < 50; i++) printf("―");
	printf("\n");

	delete[] beta;
*/




/*	smartOrbitalNew.integrate3Dcube(-10,25,-15,5,-15,10,0.2);
	smartOrbitalNew.integrate3Dcube(-10,25,-15,5,-15,10,0.1);
	smartOrbitalNew.integrate3Dcube(-10,25,-15,5,-15,10,0.05);*/

	return 0;
}




